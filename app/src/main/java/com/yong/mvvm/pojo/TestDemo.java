package com.yong.mvvm.pojo;

import java.io.Serializable;

/**
 * @PackAge：com.yong.mvvm.pojo
 * @author： yong
 * @Desc：测试demo
 * @Time: 2022/01/25 09:59:29
 **/
class TestDemo implements Serializable {
    private String name;
    private String type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
