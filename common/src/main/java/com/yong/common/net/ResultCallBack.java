package com.yong.common.net;

/**
 * @Description: 自定义返回参数接口
 * @Author: yong
 * @time 2020/7/6 10:08
 * @Version: 1.0
 */
public interface ResultCallBack<T> {
    /**
     * 获取返回参数 同时进行判断操作 判断请求状态
     * @param result
     */
    void resultCallBack(T result);

    /**
     * 异常
     * @param t
     */
    void resultThrowableCallBack(Throwable t);
}
