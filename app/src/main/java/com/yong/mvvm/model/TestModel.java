package com.yong.mvvm.model;

import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.yong.mvvm.domain.TestRequest;

/**
 * @PackAge：com.yong.mvvm.model
 * @author： yong
 * @Desc：测试model
 * @Time: 2022/01/25 11:07:21
 **/
public class TestModel extends ViewModel {

    public final MutableLiveData<String> name = new MediatorLiveData<>();

    public final TestRequest musicRequest = new TestRequest();
}
