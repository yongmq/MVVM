package com.yong.buglyutil

import android.app.Activity
import android.content.Context
import com.tencent.bugly.Bugly
import com.tencent.bugly.beta.Beta
import com.tencent.bugly.beta.UpgradeInfo
import java.lang.StringBuilder


/**
 *
 * @PackAge：com.yong.buglyutil
 * @Author： yong
 * @Desc：bugly工具类
 * @Time: 2022/02/16 15:54:56
 *
 **/
object BuglyUtil {
    public const val TAG = "BuglyUtil"

    /**
     * 获取bugly实体类
     */
    @JvmStatic
    fun getBuglyBeta(): Beta {
        return Beta.getInstance()
    }

    /**
     * bugly初始化
     * @param context
     * @param key 唯一值
     * @param boolean 是否开启debug模式，true表示打开debug模式，false表示关闭调试模式
     */
    @JvmStatic
    fun init(context: Context, key: String, boolean: Boolean) {
        Bugly.init(context, key, boolean)
    }

    /**
     * 是否启用自定义升级布局 必须定义的属性
     * 地址：（https://bugly.qq.com/docs/user-guide/advance-features-android-beta/?v=20160824161206#ui）
     *     public static final String TAG_IMG_BANNER = "beta_upgrade_banner";
     *     public static final String TAG_TITLE = "beta_title";
     *     public static final String TAG_UPGRADE_INFO = "beta_upgrade_info";
     *     public static final String TAG_UPGRADE_FEATURE = "beta_upgrade_feature";
     *     public static final String TAG_CANCEL_BUTTON = "beta_cancel_button";
     *     public static final String TAG_CONFIRM_BUTTON = "beta_confirm_button";
     *     public static final String TAG_TIP_MESSAGE = "beta_tip_message";
     */
    @JvmStatic
    fun enableCusUpgradeDialog() {
        //注意要设置在bugly init之前(自定义升级弹窗)
        Beta.upgradeDialogLayoutId = R.layout.upgrade_dialog
    }


    /**
     * 是否开启自动检查(默认为true)
     */
    @JvmStatic
    fun autoCheckUpgrade(boolean: Boolean) {
        Beta.autoCheckUpgrade = boolean
    }

    /**
     * 哪些页面可以显示升级弹窗
     * 例如，只允许在MainActivity上显示更新弹窗，其他activity上不显示弹窗; 如果不设置默认所有activity都可以显示弹窗。
     */
    @JvmStatic
    fun canShowUpgradeActs(cla: Class<Activity>) {
        Beta.canShowUpgradeActs.add(cla)
    }

    /**
     * 哪些页面不可以显示升级弹窗
     */
    @JvmStatic
    fun canNotShowUpgradeActs(cla: Class<Activity>) {
        Beta.canNotShowUpgradeActs.add(cla)
    }

    /**
     * 检查是否有更新信息
     */
    @JvmStatic
    fun checkAppUpgrade() {
        //检查更新
        Beta.checkAppUpgrade()
    }

    /**
     * 获取升级信息
     */
    @JvmStatic
    fun getUpgradeInfo(): UpgradeInfo {
        return Beta.getUpgradeInfo()
    }

    /**
     * 获取升级信息详情
     */
    @JvmStatic
    fun getUpgradeInfoDetail(): String {
        /***** 获取升级信息 *****/
        val upgradeInfo = Beta.getUpgradeInfo() ?: return "无升级信息"

        val info = StringBuilder()
        info.append("id: ").append(upgradeInfo.id).append("\n")
        info.append("标题: ").append(upgradeInfo.title).append("\n")
        info.append("升级说明: ").append(upgradeInfo.newFeature).append("\n")
        info.append("versionCode: ").append(upgradeInfo.versionCode).append("\n")
        info.append("versionName: ").append(upgradeInfo.versionName).append("\n")
        info.append("发布时间: ").append(upgradeInfo.publishTime).append("\n")
        info.append("安装包Md5: ").append(upgradeInfo.apkMd5).append("\n")
        info.append("安装包下载地址: ").append(upgradeInfo.apkUrl).append("\n")
        info.append("安装包大小: ").append(upgradeInfo.fileSize).append("\n")
        info.append("弹窗间隔（ms）: ").append(upgradeInfo.popInterval).append("\n")
        info.append("弹窗次数: ").append(upgradeInfo.popTimes).append("\n")
        info.append("发布类型（0:测试 1:正式）: ").append(upgradeInfo.publishType).append("\n")
        info.append("弹窗类型（1:建议 2:强制 3:手工）: ").append(upgradeInfo.upgradeType).append("\n")
        info.append("图片地址：").append(upgradeInfo.imageUrl)

        return info.toString()
    }


}