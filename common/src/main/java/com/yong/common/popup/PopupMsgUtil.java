package com.yong.common.popup;

import android.content.Context;
import android.view.Gravity;

/**
 * @version 1.0
 * @Description: 消息框(工具类)
 * @Author: yong
 * @time 2021/6/22 0022 14:37
 */
public class PopupMsgUtil {
    public static PopupMsg showPopupWindow(Context context, int layoutId) {
        PopupMsg popupMsg = PopupMsg.getInstance(context, layoutId);
        popupMsg.setPopupGravity(Gravity.CENTER);
        popupMsg.showPopupWindow();
        return popupMsg;
    }
}
