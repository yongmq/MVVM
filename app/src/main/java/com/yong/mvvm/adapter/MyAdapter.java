package com.yong.mvvm.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tamsiree.rxkit.RxActivityTool;
import com.yong.common.adapter.BaseAdapter;
import com.yong.device_study.adapter.MyAdapterType;
import com.yong.mvvm.R;
import com.yong.mvvm.pojo.ModelDemo;

import java.util.List;

/**
 * @PackAge：com.yong.device_study.adapter
 * @Author： yong
 * @Desc：列表适配器
 * @Time: 2022/02/28 16:48:47
 **/
public class MyAdapter<T> extends BaseAdapter<T> {
    /**
     * 根据type选择布局方式
     *
     * @param layoutResId
     * @param data
     * @param context
     * @param type
     */
    public MyAdapter(int layoutResId, @Nullable List<T> data, Context context, int type) {
        super(layoutResId, data, context, type);
    }

    @Override
    protected void convert(BaseViewHolder helper, T item) {
        super.convert(helper, item);
        switch (type) {
            case MyAdapterType.MAIN_TYPE:
                //首页列表
                mainList(helper, item);
                break;
        }
    }

    /**
     * 首页列表
     *
     * @param helper
     * @param item
     */
    private void mainList(BaseViewHolder helper, T item) {
        //首页列表
        ModelDemo modelDemo = (ModelDemo) item;

        TextView tvTitle = helper.itemView.findViewById(R.id.tv_name);
        ImageView imageView = helper.itemView.findViewById(R.id.imageView);
        CardView cardView = helper.itemView.findViewById(R.id.ll_root);

        //功能名称
        tvTitle.setText(modelDemo.getName());
        //功能图标
        Glide.with(context).load(modelDemo.getImage())
                .error(R.mipmap.pl)
                .into(imageView);
        //点击
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RxActivityTool.skipActivity(context, modelDemo.getActivity());
            }
        });
    }
}
