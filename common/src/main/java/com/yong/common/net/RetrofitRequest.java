package com.yong.common.net;

import com.yong.common.net.pojo.ApiResult;
import com.yong.common.net.configs.Constant;
import com.yong.common.utils.SPUtils;
import java.util.List;
import java.util.Map;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * @Description: http请求具体方法
 * @Author: yong
 * @time 2020/7/6 10:03
 * @Version: 1.0
 */
public class RetrofitRequest {
    private static String TAG = "RetrofitRequest";

    /**
     * post请求
     *
     * @param requestBody
     * @param url
     * @param callBack
     */
    public static void post(String url, RequestBody requestBody, BaseRetrofitHttpCallBack<ApiResult<Object>> callBack) {
        RetrofitUtil
                .getInstance()
                .getApiService(RetrofitServiceApi.class)
                .post(SPUtils.getInstance().getString(Constant.TOKEN), requestBody, url)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ApiResult<Object>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ApiResult<Object> result) {
                        //给日志添加返回结果
                        callBack.resultCallBack(result);
                    }

                    @Override
                    public void onError(Throwable e) {
                        //给日志添加返回结果
                        callBack.resultThrowableCallBack(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    /**
     * post请求
     *
     * @param requestBody
     * @param url
     * @param callBack
     */
    public static void post(String url, String parameters, RequestBody requestBody, BaseRetrofitHttpCallBack<ApiResult<Object>> callBack) {
        RetrofitUtil
                .getInstance()
                .getApiService(RetrofitServiceApi.class)
                .post(SPUtils.getInstance().getString(Constant.TOKEN), requestBody, url, parameters)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ApiResult<Object>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ApiResult<Object> result) {
                        //给日志添加返回结果
                        callBack.resultCallBack(result);
                    }

                    @Override
                    public void onError(Throwable e) {
                        //给日志添加返回结果
                        callBack.resultThrowableCallBack(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    /**
     * put请求
     *
     * @param requestBody
     * @param url
     * @param callBack
     */
    public static void put(String url, RequestBody requestBody, BaseRetrofitHttpCallBack<ApiResult<Object>> callBack) {

        RetrofitUtil
                .getInstance()
                .getApiService(RetrofitServiceApi.class)
                .put(SPUtils.getInstance().getString(Constant.TOKEN), requestBody, url)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ApiResult<Object>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ApiResult<Object> result) {
                        callBack.resultCallBack(result);
                    }

                    @Override
                    public void onError(Throwable e) {
                        callBack.resultThrowableCallBack(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    /**
     * put请求 parameters
     *
     * @param requestBody
     * @param url
     * @param callBack
     */
    public static void put(String url, String parameters, RequestBody requestBody, BaseRetrofitHttpCallBack<ApiResult<Object>> callBack) {

        RetrofitUtil
                .getInstance()
                .getApiService(RetrofitServiceApi.class)
                .put(SPUtils.getInstance().getString(Constant.TOKEN), requestBody, url, parameters)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ApiResult<Object>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ApiResult<Object> result) {
                        callBack.resultCallBack(result);
                    }

                    @Override
                    public void onError(Throwable e) {
                        callBack.resultThrowableCallBack(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    /**
     * put请求 map
     *
     * @param requestBody
     * @param url
     * @param callBack
     */
    public static void put(String url, Map<String, Object> map, RequestBody requestBody, BaseRetrofitHttpCallBack<ApiResult<Object>> callBack) {

        RetrofitUtil
                .getInstance()
                .getApiService(RetrofitServiceApi.class)
                .put(SPUtils.getInstance().getString(Constant.TOKEN), requestBody, url, map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ApiResult<Object>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ApiResult<Object> result) {
                        callBack.resultCallBack(result);
                    }

                    @Override
                    public void onError(Throwable e) {
                        callBack.resultThrowableCallBack(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    /**
     * del请求 map
     *
     * @param requestBody
     * @param url
     * @param callBack
     */
    public static void del(String url, Map<String, Object> map, RequestBody requestBody, BaseRetrofitHttpCallBack<ApiResult<Object>> callBack) {
        RetrofitUtil
                .getInstance()
                .getApiService(RetrofitServiceApi.class)
                .del(SPUtils.getInstance().getString(Constant.TOKEN), requestBody, url, map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ApiResult<Object>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ApiResult<Object> result) {
                        callBack.resultCallBack(result);
                    }

                    @Override
                    public void onError(Throwable e) {
                        callBack.resultThrowableCallBack(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    /**
     * post请求上传多张图片
     *
     * @param multipartFiles
     * @param url
     * @param callBack
     */
    public static void uploadMultipleImages(List<MultipartBody.Part> multipartFiles, String url, BaseRetrofitHttpCallBack<ApiResult<Object>> callBack) {
        String descriptionString = "This is a description";
        RequestBody description = RequestBody.create(MediaType.parse("multipart/form-data"), descriptionString);
        RetrofitUtil
                .getInstance()
                .getApiService(RetrofitServiceApi.class)
                .uploadMultipleImages(SPUtils.getInstance().getString(Constant.TOKEN), url, description, multipartFiles)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ApiResult<Object>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ApiResult<Object> result) {
                        callBack.resultCallBack(result);
                    }

                    @Override
                    public void onError(Throwable e) {
                        callBack.resultThrowableCallBack(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    /**
     * get请求(parameters)
     *
     * @param parameters
     * @param url
     * @param callBack
     */
    public static void get(String url, String parameters, BaseRetrofitHttpCallBack<ApiResult<Object>> callBack) {
        RetrofitUtil
                .getInstance()
                .getApiService(RetrofitServiceApi.class)
                .get(SPUtils.getInstance().getString(Constant.TOKEN), url, parameters)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ApiResult<Object>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ApiResult<Object> result) {
                        callBack.resultCallBack(result);
                    }

                    @Override
                    public void onError(Throwable e) {
                        callBack.resultThrowableCallBack(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    /**
     * get请求(map)
     *
     * @param url
     * @param map
     * @param callBack
     */
    public static void get(String url, Map<String, Object> map, BaseRetrofitHttpCallBack<ApiResult<Object>> callBack) {
        RetrofitUtil
                .getInstance()
                .getApiService(RetrofitServiceApi.class)
                .get(SPUtils.getInstance().getString(Constant.TOKEN), url, map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ApiResult<Object>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ApiResult<Object> result) {
                        callBack.resultCallBack(result);
                    }

                    @Override
                    public void onError(Throwable e) {
                        callBack.resultThrowableCallBack(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    /**
     * get请求(...)
     *
     * @param url
     * @param strings
     * @param callBack
     */
    public static void get(String url, String[] strings, BaseRetrofitHttpCallBack<ApiResult<Object>> callBack) {
        RetrofitUtil
                .getInstance()
                .getApiService(RetrofitServiceApi.class)
                .get(SPUtils.getInstance().getString(Constant.TOKEN), url, strings)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ApiResult<Object>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ApiResult<Object> result) {
                        callBack.resultCallBack(result);
                    }

                    @Override
                    public void onError(Throwable e) {
                        callBack.resultThrowableCallBack(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    /**
     * get请求(Response)
     *
     * @param url
     * @param callBack
     */
    public static void getResponse(String url, BaseRetrofitHttpCallBack<ApiResult<Response>> callBack) {
        RetrofitUtil
                .getInstance()
                .getApiService(RetrofitServiceApi.class)
                .getResponse(SPUtils.getInstance().getString(Constant.TOKEN), url)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<retrofit2.Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(retrofit2.Response<ResponseBody> result) {
                        ApiResult<Response> responseApiResult = new ApiResult<>();
                        responseApiResult.setCode(result.code());
                        responseApiResult.setMsg(result.message());
                        if (result.code() == 200) {
                            callBack.resultCallBack(responseApiResult);
                        } else {
                            callBack.onCodeFailure(result.code(), responseApiResult, result.message());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        callBack.resultThrowableCallBack(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
