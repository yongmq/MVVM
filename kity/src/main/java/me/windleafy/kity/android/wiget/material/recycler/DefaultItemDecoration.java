package me.windleafy.kity.android.wiget.material.recycler;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;


import me.windleafy.kity.R;

/**
 * RecyclerView分隔线
 */
public class DefaultItemDecoration extends DividerItemDecoration {

    public DefaultItemDecoration(Context context) {
        super(context, DividerItemDecoration.VERTICAL);
        setDrawable(ContextCompat.getDrawable(context, R.drawable.kity_line_divider_1px));
    }

    public DefaultItemDecoration(Context context,int drawable) {
        super(context, DividerItemDecoration.VERTICAL);
        setDrawable(ContextCompat.getDrawable(context, drawable));
    }

}
