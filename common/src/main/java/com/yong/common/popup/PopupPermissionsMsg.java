package com.yong.common.popup;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.TextView;

import com.yong.common.R;

import razerdp.basepopup.BasePopupWindow;
import razerdp.util.animation.AnimationHelper;
import razerdp.util.animation.ScaleConfig;

/**
 * @version 1.0
 * @Description: 权限申请消息框
 * @Author: yong
 * @time 2021/6/22 0022 19:05
 */
public class PopupPermissionsMsg extends BasePopupWindow {

    public PopupPermissionsMsg(Context context, String qrCode) {
        super(context);
        //消息提示
        TextView tvEndPieceQr = findViewById(R.id.tv_permissions_msg);
        if (qrCode != null && !"".equals(qrCode)) {
            tvEndPieceQr.setText(qrCode);
        }

        //去设置
        Button btn = findViewById(R.id.btn_setting);
        btn.setOnClickListener(view -> {
            Intent intent = new Intent();
            //下面这种方案是直接跳转到当前应用的设置界面。
            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            Uri uri = Uri.fromParts("package", context.getPackageName(), null);
            intent.setData(uri);
            context.startActivity(intent);
        });

        //取消
        Button btnCancel = findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(view -> {
            dismiss();
        });

        setPopupGravity(Gravity.CENTER);
    }

    /**
     *
     * @param context
     * @param qrCode
     * @param isHide 是否隐藏底部按钮
     */
    public PopupPermissionsMsg(Context context, String qrCode, boolean isHide) {
        super(context);
        //消息提示
        TextView tvEndPieceQr = findViewById(R.id.tv_permissions_msg);
        if (qrCode != null && !"".equals(qrCode)) {
            tvEndPieceQr.setText(qrCode);
        }

        //去设置
        Button btn = findViewById(R.id.btn_setting);
        btn.setOnClickListener(view -> {
            Intent intent = new Intent();
            //下面这种方案是直接跳转到当前应用的设置界面。
            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            Uri uri = Uri.fromParts("package", context.getPackageName(), null);
            intent.setData(uri);
            context.startActivity(intent);
        });

        //取消
        Button btnCancel = findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(view -> {
            dismiss();
        });

        setPopupGravity(Gravity.CENTER);

        //隐藏按钮
        if (isHide){
            btn.setVisibility(View.GONE);
            btnCancel.setVisibility(View.GONE);
        }
    }

    @Override
    protected Animation onCreateShowAnimation() {
        return AnimationHelper.asAnimation()
                .withScale(ScaleConfig.CENTER)
                .toShow();
    }

    @Override
    protected Animation onCreateDismissAnimation() {
        return AnimationHelper.asAnimation()
                .withScale(ScaleConfig.CENTER)
                .toDismiss();
    }


    @Override
    public View onCreateContentView() {
        return createPopupById(R.layout.popup_permissions_msg);
    }
}
