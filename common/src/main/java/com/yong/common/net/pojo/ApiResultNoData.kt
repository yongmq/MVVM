package com.yong.common.net.pojo

import java.io.Serializable

/**
 * @Description: 没有data的数据返回
 * @Author: yong
 * @time 2020/7/2 10:56
 * @Version: 1.0
 */
open class ApiResultNoData : Serializable {
     var code = 0
     var msg: String? = null
}