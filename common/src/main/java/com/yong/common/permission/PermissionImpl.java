package com.yong.common.permission;

/**
 * @version 1.0
 * @Description: 权限请求通过
 * @Author: yong
 * @time 2021/8/20 0020 18:40
 */
public interface PermissionImpl {
    /**
     * 成功
     */
    default void onSuccess(){}
}
