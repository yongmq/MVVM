package com.yong.common.net.presenter;

import android.app.Activity;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.yong.common.net.BaseRetrofitHttpCallBack;
import com.yong.common.net.RetrofitRequest;
import com.yong.common.net.contract.BaseContract;
import com.yong.common.net.pojo.ApiResult;
import com.yong.common.net.pojo.RecordsBean;
import com.yong.common.net.configs.Constant;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * @version 1.0
 * @Description:
 * @Author: yong
 * @time 2021/1/16 0016 16:07
 */
public class BasePresenter implements BaseContract.BasePresenter {
    private BaseContract.BaseViewCallBack baseViewCallBack;

    public BasePresenter(BaseContract.BaseViewCallBack callBack) {
        this.baseViewCallBack = callBack;
    }

    @NotNull
    @Override
    public List<Object> placeholder() {
        List<Object> objects = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            objects.add(null);
        }
        return objects;
    }


    @Override
    public void get(@NotNull String url, @Nullable String parameters, int type, Activity activity, boolean b) {
        RetrofitRequest.get(url, parameters, new BaseRetrofitHttpCallBack<ApiResult<Object>>(activity, b) {
            @Override
            public void onCodeSuccess(ApiResult<Object> data) {
                if (type == Constant.NO_DATA_PROCESSING) {
                    baseViewCallBack.onSuccess(data);
                } else {
                    dataProcessing(data, type);
                }
            }

            @Override
            protected void onError() {
                baseViewCallBack.onError(type);
            }

            @Override
            protected void onFinish() {
                baseViewCallBack.onFinish();
            }
        });
    }

    @Override
    public void get(@NotNull String url, @Nullable Map<String, ?> map, int type, @NotNull Activity Activity, boolean b) {
        RetrofitRequest.get(url, (Map<String, Object>) map, new BaseRetrofitHttpCallBack<ApiResult<Object>>(Activity, b) {
            @Override
            public void onCodeSuccess(ApiResult<Object> data) {
                if (type == Constant.NO_DATA_PROCESSING) {
                    baseViewCallBack.onSuccess(data);
                } else {
                    dataProcessing(data, type);
                }
            }

            @Override
            protected void onError() {
                baseViewCallBack.onError(type);
            }

            @Override
            protected void onFinish() {
                baseViewCallBack.onFinish();
            }
        });
    }

    @Override
    public void get(@NotNull String url, @Nullable String[] strings, int type, @NotNull Activity Activity, boolean b) {
        RetrofitRequest.get(url, strings, new BaseRetrofitHttpCallBack<ApiResult<Object>>(Activity, b) {
            @Override
            public void onCodeSuccess(ApiResult<Object> data) {
                if (type == Constant.NO_DATA_PROCESSING) {
                    baseViewCallBack.onSuccess(data);
                } else {
                    dataProcessing(data, type);
                }
            }

            @Override
            protected void onError() {

                baseViewCallBack.onError(type);
            }

            @Override
            protected void onFinish() {
                baseViewCallBack.onFinish();
            }
        });
    }

    @Override
    public void post(@NotNull String url, @Nullable Object requestBody, int type, @NotNull Activity Activity, boolean b) {
        String json = new Gson().toJson(requestBody);
        RequestBody body = RequestBody.create(MediaType.parse("application/json"), json);
        RetrofitRequest.post(url, body, new BaseRetrofitHttpCallBack<ApiResult<Object>>(Activity, b) {
            @Override
            public void onCodeSuccess(ApiResult<Object> data) {
                if (type == Constant.NO_DATA_PROCESSING) {
                    baseViewCallBack.onSuccess(data);
                } else {
                    dataProcessing(data, type);
                }
            }

            @Override
            protected void onError() {

                baseViewCallBack.onError(type);
            }

            @Override
            protected void onFinish() {
                baseViewCallBack.onFinish();
            }
        });
    }

    @Override
    public void post(@NotNull String url, @Nullable String parameters, @Nullable Object requestBody, int type, @NotNull Activity Activity, boolean b) {
        String json = new Gson().toJson(requestBody);
        RequestBody body = RequestBody.create(MediaType.parse("application/json"), json);
        RetrofitRequest.post(url, parameters, body, new BaseRetrofitHttpCallBack<ApiResult<Object>>(Activity, b) {
            @Override
            public void onCodeSuccess(ApiResult<Object> data) {
                if (type == Constant.NO_DATA_PROCESSING) {
                    baseViewCallBack.onSuccess(data);
                } else {
                    dataProcessing(data, type);
                }
            }

            @Override
            protected void onError() {

                baseViewCallBack.onError(type);
            }

            @Override
            protected void onFinish() {
                baseViewCallBack.onFinish();
            }
        });
    }

    @Override
    public void put(@NotNull String url, @Nullable Object requestBody, int type, @NotNull Activity Activity, boolean b) {
        String json = new Gson().toJson(requestBody);
        RequestBody body = RequestBody.create(MediaType.parse("application/json"), json);
        RetrofitRequest.put(url, body, new BaseRetrofitHttpCallBack<ApiResult<Object>>(Activity, b) {
            @Override
            public void onCodeSuccess(ApiResult<Object> data) {
                if (type == Constant.NO_DATA_PROCESSING) {
                    baseViewCallBack.onSuccess(data);
                } else {
                    dataProcessing(data, type);
                }
            }

            @Override
            protected void onError() {

                baseViewCallBack.onError(type);
            }

            @Override
            protected void onFinish() {
                baseViewCallBack.onFinish();
            }
        });
    }

    @Override
    public void put(@NotNull String url, @Nullable String parameters, @Nullable Object requestBody, int type, @NotNull Activity Activity, boolean b) {
        String json = new Gson().toJson(requestBody);
        RequestBody body = RequestBody.create(MediaType.parse("application/json"), json);
        RetrofitRequest.put(url, parameters, body, new BaseRetrofitHttpCallBack<ApiResult<Object>>(Activity, b) {
            @Override
            public void onCodeSuccess(ApiResult<Object> data) {
                if (type == Constant.NO_DATA_PROCESSING) {
                    baseViewCallBack.onSuccess(data);
                } else {
                    dataProcessing(data, type);
                }
            }

            @Override
            protected void onError() {
                baseViewCallBack.onError(type);
            }

            @Override
            protected void onFinish() {
                baseViewCallBack.onFinish();
            }
        });
    }

    @Override
    public void put(@NotNull String url, @Nullable Map<String, ?> map, @Nullable Object requestBody, int type, @NotNull Activity Activity, boolean b) {
        String json = new Gson().toJson(requestBody);
        RequestBody body = RequestBody.create(MediaType.parse("application/json"), json);
        RetrofitRequest.put(url, (Map<String, Object>) map, body, new BaseRetrofitHttpCallBack<ApiResult<Object>>(Activity, b) {
            @Override
            public void onCodeSuccess(ApiResult<Object> data) {
                if (type == Constant.NO_DATA_PROCESSING) {
                    baseViewCallBack.onSuccess(data);
                } else {
                    dataProcessing(data, type);
                }
            }

            @Override
            protected void onError() {
                baseViewCallBack.onError(type);
            }

            @Override
            protected void onFinish() {
                baseViewCallBack.onFinish();
            }
        });
    }

    @Override
    public void del(@NotNull String url, @Nullable Map<String, ?> map, @Nullable Object requestBody, int type, @NotNull Activity Activity, boolean b) {
        String json = new Gson().toJson(requestBody);
        RequestBody body = RequestBody.create(MediaType.parse("application/json"), json);
        RetrofitRequest.del(url, (Map<String, Object>) map, body, new BaseRetrofitHttpCallBack<ApiResult<Object>>(Activity, b) {
            @Override
            public void onCodeSuccess(ApiResult<Object> data) {
                if (type == Constant.NO_DATA_PROCESSING) {
                    baseViewCallBack.onSuccess(data);
                } else {
                    dataProcessing(data, type);
                }
            }

            @Override
            protected void onError() {
                baseViewCallBack.onError(type);
            }

            @Override
            protected void onFinish() {
                baseViewCallBack.onFinish();
            }
        });
    }

    /**
     * 数据处理
     *
     * @param data
     * @param type
     */
    public void dataProcessing(ApiResult<Object> data, int type) {

    }
}
