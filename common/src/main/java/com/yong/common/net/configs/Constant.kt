package com.yong.common.net.configs

/**
 * @Description:请求url
 * @Author:         yong
 * @time            2020/8/21 18:55
 * @Version:        1.0
 */
class Constant {
    companion object {
        //默认地址
        lateinit var BASE_URL: String

        /**
         * token
         */
        const val TOKEN = "token"

        /**
         * 返回原始数据
         */
        const val NO_DATA_PROCESSING = -1

    }
}