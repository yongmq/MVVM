package com.yong.common.utils;

import android.widget.TextView;

import androidx.annotation.NonNull;

import org.jetbrains.annotations.Contract;

/**
 * @PackAge：com.gaokao.jhapp.yong.utils
 * @author： yong
 * @Desc：TextView工具类
 * @Time: 2022/02/14 09:23:17
 **/
public class TextViewUtil {
    /**
     * 设置默认值
     * 默认值是-
     */
    public static void setDefLine(@NonNull TextView textView, Object o) {
        String str = strFormat(o, "-");
        textView.setText(str);
    }

    /**
     * 设置自定义默认值
     */
    public static void setCusDef(@NonNull TextView textView, Object o, String def) {
        String str = strFormat(o, def);
        textView.setText(str);
    }

    /**
     * 字符串格式化
     */
    @NonNull
    @Contract(pure = true)
    public static String strFormat(Object o, String str) {
        if (o == null) {
            return str;
        } else if ("null".equals(o) || "".equals(o)) {
            return str;
        } else if (o instanceof String) {
            return o.toString();
        } else if (o instanceof Integer) {
            return "0";
        } else {
            return o.toString();
        }
    }


}
