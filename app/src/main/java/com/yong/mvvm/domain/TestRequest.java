package com.yong.mvvm.domain;

import androidx.annotation.Nullable;
import androidx.lifecycle.DefaultLifecycleObserver;

import com.yong.common.net.callback.ProtectedUnPeekLiveData;
import com.yong.common.net.callback.UnPeekLiveData;
import com.yong.common.net.contract.BaseContract;
import com.yong.common.net.pojo.ApiResult;
import com.yong.common.net.presenter.BasePresenter;
import com.yong.common.utils.ToastUtils;
import com.yong.device_study.ui.MainActivity;

/**
 * @PackAge：com.yong.mvvm.domain
 * @author： yong
 * @Desc：测试请求
 * @Time: 2022/01/25 11:11:50
 **/
public class TestRequest implements DefaultLifecycleObserver {

    //TODO tip：👆👆👆 让 accountRequest 可观察页面生命周期，
    // 从而在页面即将退出、且登录请求由于网络延迟尚未完成时，
    // 及时通知数据层取消本次请求，以避免资源浪费和一系列不可预期的问题。

    private final UnPeekLiveData<ApiResult<Object>> tokenLiveData = new UnPeekLiveData<>();

    //TODO tip 2：向 ui 层提供的 request LiveData，使用 "父类的 LiveData" 而不是 "Mutable 的 LiveData"，
    //如此达成了 "唯一可信源" 的设计，也即通过访问控制权限实现 "读写分离"，
    //并且进一步地，使用 ProtectedUnPeekLiveData 类，而不是 LiveData 类，
    //以此来确保消息分发的可靠一致，及 "事件" 场景下的防倒灌特性。

    //如果这样说还不理解的话，详见《关于 LiveData 本质，你看到了第几层》的铺垫和解析。
    //https://xiaozhuanlan.com/topic/6017825943

    public ProtectedUnPeekLiveData<ApiResult<Object>> getTokenLiveData() {

        //TODO tip 3：与此同时，为了方便语义上的理解，故而直接将 DataResult 作为 LiveData value 回推给 UI 层，
        //而不是将 DataResult 的泛型实体拆下来单独回推，如此
        //一方面使 UI 层有机会基于 DataResult 的 responseStatus 来分别处理 请求成功或失败的情况下的 UI 表现，
        //另一方面从语义上强调了 该数据是请求得来的结果，是只读的，与 "可变状态" 形成明确的区分，
        //从而方便团队开发人员自然而然遵循 "唯一可信源"/"单向数据流" 的开发理念，规避消息同步一致性等不可预期的错误。

        //如果这样说还不理解的话，详见《如何让同事爱上架构模式、少写 bug 多注释》中对 "只读数据" 和 "可变状态" 的区分的解析。
        //https://xiaozhuanlan.com/topic/8204519736

        return tokenLiveData;
    }

    /**
     * 测试一个接口
     * @param mainActivity
     */
    public void requestTest(MainActivity mainActivity) {
        BasePresenter basePresenter = new BasePresenter(new BaseContract.BaseViewCallBack() {
            @Override
            public void onSuccess(@Nullable ApiResult<Object> data) {
                tokenLiveData.postValue(null);
            }

            @Override
            public void onError(int type) {
                tokenLiveData.postValue(new ApiResult<>());
            }

            @Override
            public void onFinish() {
                ToastUtils.showLongToast(null,"测试");
            }
        });
        basePresenter.get("", (String) null,-1, mainActivity,true);
    }

}
