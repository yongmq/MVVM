package com.yong.mvvm.utils.list;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;


import com.yong.mvvm.R;

import me.windleafy.kity.android.utils.InflaterKit;


public class NoticeUnit {


    private View mNoticeView;

    public NoticeUnit(Activity activity, int noticeResId) {
        mNoticeView = InflaterKit.inflate(activity, noticeResId);
    }

    public NoticeUnit(Fragment fragment, int noticeResId) {
        mNoticeView = InflaterKit.inflate(fragment, noticeResId);
    }

    private void setVisibility(boolean linearlayout, boolean textonly) {
        LinearLayout layout = mNoticeView.findViewById(R.id.no_collect_layout);
        layout.setVisibility(linearlayout ? View.VISIBLE : View.GONE);
        TextView textView = mNoticeView.findViewById(R.id.tv_only);
        textView.setVisibility(textonly ? View.VISIBLE : View.GONE);
    }

    public NoticeUnit setNoticeText(CharSequence text) {
        setVisibility(true, false);
        TextView textView = mNoticeView.findViewById(R.id.tv_def_content);
        textView.setText(text);
        return this;
    }


    public NoticeUnit setEmptyImage(int image_resid) {
        setVisibility(true, false);
        ImageView textView = mNoticeView.findViewById(R.id.image);
        textView.setImageResource(image_resid);
        return this;
    }

    public NoticeUnit setNotice(int imageResid, CharSequence text) {
        setEmptyImage(imageResid);
        setNoticeText(text);
        return this;
    }

    public NoticeUnit setNoticeTextOnly(CharSequence text) {
        setVisibility(false, true);
        TextView textView = mNoticeView.findViewById(R.id.tv_only);
        textView.setText(text);
        return this;
    }

    //EmptyText
    public NoticeUnit setNoticeText(boolean show, CharSequence text) {
        setNoticeTextOnly(text);
        return this;
    }

    public View getNoticeView() {
        return mNoticeView;
    }

}

