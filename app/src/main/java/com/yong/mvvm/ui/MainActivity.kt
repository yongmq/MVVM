package com.yong.device_study.ui

import com.yong.common.ui.page.BaseActivity
import com.yong.common.ui.page.config.DataBindingConfig
import com.yong.device_study.adapter.MyAdapterType
import com.yong.mvvm.BR
import com.yong.mvvm.R
import com.yong.mvvm.adapter.MyAdapter
import com.yong.mvvm.databinding.ActivityMainBinding
import com.yong.mvvm.state.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.collections.ArrayList

/**
 * @PackAge：com.yong.device_study.ui
 * @Author： yong
 * @Desc：首页
 * @Time: 2022/03/01 17:12:36
 **/
class MainActivity : BaseActivity() {
    private lateinit var mainViewModel: MainViewModel
    private lateinit var activityMainBinding: ActivityMainBinding

    /**
     * 列表适配器
     */
    private var myAdapter: MyAdapter<Any>? = null
    private var modelDemo: MutableList<Any> = ArrayList()

    override fun initViewModel() {
        mainViewModel = getActivityScopeViewModel(MainViewModel::class.java)
    }

    override fun getDataBindingConfig(): DataBindingConfig {
        return DataBindingConfig(R.layout.activity_main, BR.mainModel, mainViewModel)
    }

    override fun initBinding() {
        activityMainBinding = binding as ActivityMainBinding
    }

    /**
     * 初始化数据
     */
    override fun initData() {
        mainViewModel.generateTestData()

        mainViewModel.list.observe(this) {
            modelDemo.addAll(it)
            //更新列表
            updateList()
        }
    }

    /**
     * 更新列表
     */
    private fun updateList() {
        if (myAdapter != null) {
            myAdapter?.notifyDataSetChanged()
        } else {
            myAdapter = MyAdapter(R.layout.item_main, modelDemo, this, MyAdapterType.MAIN_TYPE)
            activityMainBinding.recycleView.adapter = myAdapter
        }
        myAdapter?.setEmptyView(R.layout.load_data_empty, recycle_view)
    }
}