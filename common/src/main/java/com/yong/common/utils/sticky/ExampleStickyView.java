package com.yong.common.utils.sticky;

import android.view.View;

/**
 * @PackAge：com.gaokao.jhapp.yong.sticky
 * @author： yong
 * @Desc： 吸附布局
 * @Time: 2022/02/11 10:19:41
 **/
public class ExampleStickyView implements StickyView {

    @Override
    public boolean isStickyView(View view) {
        if (view == null) {
            return false;
        } else if (view.getTag() == null) {
            return false;
        } else {
            return (Boolean) view.getTag();
        }
    }

    @Override
    public int getStickViewType() {
        return STICKY_VIEW_TYPE;
    }
}
