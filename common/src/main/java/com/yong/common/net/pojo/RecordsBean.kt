package com.yong.common.net.pojo

/**
 * @Description: 通用数据类（用于很多数据data里records）
 * @Author:         yong
 * @time            2020/10/10 11:20
 * @Version:        1.0
 */
class RecordsBean<T> {
     val records: List<T>? = null
}