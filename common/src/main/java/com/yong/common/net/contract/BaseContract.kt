package com.yong.common.net.contract

import android.app.Activity
import com.yong.common.net.pojo.ApiResult

/**
 * @version 1.0
 * @Description:
 * @Author: yong
 * @time 2021/1/16 0016 16:08
 */
class BaseContract {
    internal interface BasePresenter {
        //获取10条占位数据
        fun placeholder(): MutableList<Any>

        //get请求 parameters 示例：?parameters={}
        operator fun get(
            url: String,
            parameters: String?,
            type: Int,
            activity: Activity?,
            boolean: Boolean
        )

        //get请求 map 示例：?key=value
        operator fun get(
            url: String,
            map: Map<String, Any?>?,
            type: Int,
            activity: Activity?,
            boolean: Boolean
        )

        //get请求 strings 示例：?list=value&list=value
        operator fun get(
            url: String,
            strings: Array<String>?,
            type: Int,
            activity: Activity?,
            boolean: Boolean
        )

        //post请求 requestBody
        fun post(
            url: String,
            requestBody: Any?,
            type: Int,
            activity: Activity?,
            boolean: Boolean
        )

        //post请求 requestBody,parameters
        fun post(
            url: String,
            parameters: String?,
            requestBody: Any?,
            type: Int,
            activity: Activity?,
            boolean: Boolean
        )

        //put请求 requestBody
        fun put(
            url: String,
            requestBody: Any?,
            type: Int,
            activity: Activity?,
            boolean: Boolean
        )

        //put请求 requestBody,parameters
        fun put(
            url: String,
            parameters: String?,
            requestBody: Any?,
            type: Int,
            activity: Activity?,
            boolean: Boolean
        )

        //put请求 requestBody,map
        fun put(
            url: String,
            map: Map<String, Any?>?,
            requestBody: Any?,
            type: Int,
            activity: Activity?,
            boolean: Boolean
        )

        //del请求 requestBody,map
        fun del(
            url: String,
            map: Map<String, Any?>?,
            requestBody: Any?,
            type: Int,
            activity: Activity?,
            boolean: Boolean
        )
    }

    interface BaseViewCallBack {
        //未处理的数据 type=-1
        fun onSuccess(data: ApiResult<Any?>?) {
        }
        //请求失败
        fun onError(type: Int) {}

        //请求结束
        fun onFinish() {}
    }
}