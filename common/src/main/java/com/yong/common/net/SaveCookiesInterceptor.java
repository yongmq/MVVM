package com.yong.common.net;

import android.util.Log;

import com.yong.common.utils.SPUtils;

import java.io.IOException;
import java.util.HashSet;

import okhttp3.Interceptor;
import okhttp3.Response;

/**
 * @Description: cookie保存到sp
 * @Author: yong
 * @time 2020/7/27 17:09
 * @Version: 1.0
 */
public class SaveCookiesInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Response originalResponse = chain.proceed(chain.request());

        if (!originalResponse.headers("Set-Cookie").isEmpty()) {
            HashSet<String> cookies = new HashSet<>();

            for (String header : originalResponse.headers("Set-Cookie")) {
                cookies.add(header);
                Log.v("OkHttp", "save Header: " + header);
            }

            SPUtils.getInstance().put("cookie", cookies);
        }

        return originalResponse;
    }
}