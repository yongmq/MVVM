package com.yong.common.utils;

import android.content.Context;
import android.widget.Toast;

import com.yong.common.BaseApplication;

import org.jetbrains.annotations.NotNull;

/**
 * Create by KunMinX at 2021/8/19
 */
public class ToastUtils {

    public static void showLongToast(Context context, String text) {
        if (context != null) {
            Toast.makeText(context, text, Toast.LENGTH_LONG).show();
        }else {
            Toast.makeText(Utils.getApp().getApplicationContext(), text, Toast.LENGTH_LONG).show();
        }

    }

    public static void showShortToast(Context context, String text) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }
}
