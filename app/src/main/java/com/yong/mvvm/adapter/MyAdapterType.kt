package com.yong.device_study.adapter

/**
 * @PackAge：com.yong.device_study.adapter
 * @Author： yong
 * @Desc：我的列表类型
 * @Time: 2022/02/28 16:50:07
 */
object MyAdapterType {
    /**
     * 首页类型
     */
    const val MAIN_TYPE = 0x00000001
}