package com.yong.common.net;

import com.yong.common.net.pojo.ApiResult;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * @Description: 接口api
 * @Author: yong
 * @time 2020/7/2 18:16
 * @Version: 1.0
 */
public interface RetrofitServiceApi {

    /**
     * post请求
     * 1.普通put请求
     * 2.带parameters的普通
     * 3. map普通请求
     *
     * @param token
     * @param requestBody
     * @param url
     * @return
     */
    @POST("{url}")
    Observable<ApiResult<Object>> post(@Header("token") String token, @Body RequestBody requestBody, @Path(value = "url", encoded = true) String url);

    @POST("{url}")
    Observable<ApiResult<Object>> post(@Header("token") String token, @Body RequestBody requestBody, @Path(value = "url", encoded = true) String url, @Query("parameters") String parameters);

    @POST("{url}")
    Observable<ApiResult<Object>> post(@Header("token") String token, @Body RequestBody requestBody, @Path(value = "url", encoded = true) String url, @QueryMap Map<String, Object> map);

    /**
     * PUT请求
     * 1.普通put请求
     * 2.带parameters的普通
     * 3. map普通请求
     *
     * @param token
     * @param requestBody
     * @param url
     * @return
     */
    @PUT("{url}")
    Observable<ApiResult<Object>> put(@Header("token") String token, @Body RequestBody requestBody, @Path(value = "url", encoded = true) String url);

    @PUT("{url}")
    Observable<ApiResult<Object>> put(@Header("token") String token, @Body RequestBody requestBody, @Path(value = "url", encoded = true) String url, @Query("parameters") String parameters);

    @PUT("{url}")
    Observable<ApiResult<Object>> put(@Header("token") String token, @Body RequestBody requestBody, @Path(value = "url", encoded = true) String url, @QueryMap Map<String, Object> map);

    /**
     * del请求
     * 1.普通put请求
     * 2.带parameters的普通
     * 3. map普通请求
     *
     * @param token
     * @param requestBody
     * @param url
     * @return
     */
    @HTTP(method = "DELETE", path = "{url}", hasBody = true)
    Observable<ApiResult<Object>> del(@Header("token") String token, @Body RequestBody requestBody, @Path(value = "url", encoded = true) String url);

    @HTTP(method = "DELETE", path = "{url}", hasBody = true)
    Observable<ApiResult<Object>> del(@Header("token") String token, @Body RequestBody requestBody, @Path(value = "url", encoded = true) String url, @Query("parameters") String parameters);

    @HTTP(method = "DELETE", path = "{url}", hasBody = true)
    Observable<ApiResult<Object>> del(@Header("token") String token, @Body RequestBody requestBody, @Path(value = "url", encoded = true) String url, @QueryMap Map<String, Object> map);

    /**
     * get请求
     * 1.带parameters的普通
     * 2. map普通请求
     *
     * @param token
     * @param parameters
     * @param url
     * @return
     */
    @GET("{url}")
    Observable<ApiResult<Object>> get(@Header("token") String token, @Path(value = "url", encoded = true) String url, @Query(value = "parameters") String parameters);

    @GET("{url}")
    Observable<ApiResult<Object>> get(@Header("token") String token, @Path(value = "url", encoded = true) String url, @QueryMap Map<String, Object> map);
    @GET("{url}")
    Observable<ApiResult<Object>> get(@Header("token") String token, @Path(value = "url", encoded = true) String url, @Query(value = "list") String... list);
    @GET("{url}")
    Observable<Response<ResponseBody>> getResponse(@Header("token") String token, @Path(value = "url", encoded = true) String url);

    /**
     * 多张图片上传
     *
     * @param multipartFiles
     * @param token
     * @param url
     * @param requestBody
     * @return
     */
    @Multipart
    @POST("{url}")
    Observable<ApiResult<Object>> uploadMultipleImages(@Header("token") String token, @Path(value = "url", encoded = true) String url, @Part("description") RequestBody requestBody, @Part List<MultipartBody.Part> multipartFiles);

//    @Headers({"Accept: application/json", "Content-Type: application/json"})
//    @POST("echart/his/procedure")
//    Observable<APIResult<MedicineBean>> getMedicineList(@Body RequestBody requestBody);
//
//    @Headers({"Accept: application/json", "Content-Type: application/json"})
//    @POST("echart/his/procedure")
//    Observable<APIResult<ShouyaoBean>> finishShouyao(@Body RequestBody requestBody);
//
//    @GET
//    Call<ResponseBody> reqGet(@QueryMap Map<String, String> options);
//
//    @GET
//    Call<ResponseBody> reqGet();
}
