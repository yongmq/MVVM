package com.yong.mvvm.ui.function;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.yong.mvvm.R;
import com.yong.mvvm.receiver.AdminManageReceiver;

/**
 * @PackAge：com.yong.device_study.ui.function
 * @Author： yong
 * @Desc：一键锁屏
 * @Time: 2022/03/04 11:19:15
 **/
public class OneKeyLockScreenActivity extends AppCompatActivity {
    private ComponentName mAdminName = null;
    private static final String TAG = "OneKeyLockScreenActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdminName = new ComponentName(this, AdminManageReceiver.class);
        //获取设备管理器
        DevicePolicyManager mDPM = (DevicePolicyManager) this.getSystemService(Context.DEVICE_POLICY_SERVICE);
        if (!mDPM.isAdminActive(mAdminName)) {
            showAdminManage();
        } else if (mDPM.isAdminActive(mAdminName)) {
            //一键锁屏
            mDPM.lockNow();
        }

        finish();
    }

    /**
     * 显示设备管理页面
     */
    private void showAdminManage() {
        Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
        intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, mAdminName);
        intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, R.string.app_name);
        startActivityForResult(intent, -1);
    }
}
