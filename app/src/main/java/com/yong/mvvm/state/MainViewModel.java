package com.yong.mvvm.state;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.yong.mvvm.pojo.ModelDemo;
import com.yong.mvvm.ui.function.OneKeyLockScreenActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * @PackAge：com.yong.device_study.state
 * @Author： yong
 * @Desc：
 * @Time: 2022/03/01 10:16:14
 **/
public class MainViewModel extends ViewModel {
    public final MutableLiveData<List<ModelDemo>> list = new MutableLiveData<>();

    public void generateTestData() {
        List<ModelDemo> demoList = new ArrayList<>();

        demoList.add(new ModelDemo("一键锁屏", -1, OneKeyLockScreenActivity.class));
        list.postValue(demoList);
    }
}
