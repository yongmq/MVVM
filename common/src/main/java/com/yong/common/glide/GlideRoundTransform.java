package com.yong.common.glide;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;

import androidx.annotation.NonNull;

import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import com.bumptech.glide.load.resource.bitmap.TransformationUtils;
import com.bumptech.glide.util.Util;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.MessageDigest;

/**
 * @Description: 自定义圆角图片(顶部左右圆角)
 * @Author: yong
 * @time 2020/7/11 11:17
 * @Version: 1.0
 */
public class GlideRoundTransform extends BitmapTransformation {
    private static final String ID = "com.bumptech.glide.transformations.GlideRoundTransform";
    private static final byte[] ID_BYTES = ID.getBytes(Charset.forName("UTF-8"));
    private float radius = 0f;

    /**
     * 判断是否是全圆角 否的话就顶部两个圆角
     */
    private boolean isAllRadius;

    public GlideRoundTransform(int dp, boolean isAllRadius1) {
        this.isAllRadius = isAllRadius1;
        //全局改为5dp(5为瑞来默认圆角)
        //dp = 5;
        radius = Resources.getSystem().getDisplayMetrics().density * dp;
    }

    @Override
    protected Bitmap transform(@NonNull BitmapPool pool, @NonNull Bitmap toTransform, int outWidth, int outHeight) {
        //变换的时候裁切
        Bitmap bitmap = TransformationUtils.centerCrop(pool, toTransform, outWidth, outHeight);

        return roundCrop(pool, bitmap);
    }

    private Bitmap roundCrop(BitmapPool pool, Bitmap source) {
        if (source == null) {
            return null;
        }
        Bitmap result = pool.get(source.getWidth(), source.getHeight(), Bitmap.Config.ARGB_8888);
        if (result == null) {
            result = Bitmap.createBitmap(source.getWidth(), source.getHeight(), Bitmap.Config.ARGB_8888);
        }
        Canvas canvas = new Canvas(result);
        Paint paint = new Paint();
        paint.setShader(new BitmapShader(source, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP));
        paint.setAntiAlias(true);
        RectF rectF = new RectF(0f, 0f, source.getWidth(), source.getHeight());
        canvas.drawRoundRect(rectF, radius, radius, paint);
        if (!isAllRadius) {
            //只保留左上角、右上角圆角（注释掉一下两行，则四个角为圆角）
            //left为底部两个圆角 top为顶部两个圆角(不知道为啥要100f)
            RectF rectRound = new RectF(0f, 100f, source.getWidth(), source.getHeight());
            canvas.drawRect(rectRound, paint);
        }
        return result;
    }

    public String getId() {
        return getClass().getName() + Math.round(radius);
    }

    //下面三个方法需要实现，不然会出现刷新闪烁
    @Override
    public boolean equals(Object o) {
        if (o instanceof GlideRoundTransform) {
            GlideRoundTransform other = (GlideRoundTransform) o;
            return radius == other.radius;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Util.hashCode(ID.hashCode(), Util.hashCode(radius));
    }

    @Override
    public void updateDiskCacheKey(@NonNull MessageDigest messageDigest) {
        messageDigest.update(ID_BYTES);

        byte[] radiusData = ByteBuffer.allocate(4).putFloat(radius).array();
        messageDigest.update(radiusData);
    }
}

