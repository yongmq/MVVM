package com.yong.common.adapter;

import android.content.Context;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.entity.MultiItemEntity;

import java.util.List;

/**
 * @version 1.0
 * @Description: 默认多布局
 * @Author: yong
 * @time 2021/1/19 0019 11:01
 */
public class BaseMultiItemAdapter<T extends MultiItemEntity> extends BaseMultiItemQuickAdapter<T, BaseViewHolder> {
    private final static String TAG = "BaseMultiItemAdapter";
    /**
     * 上下文
     */
    private Context context;

    /**
     * 默认启用占位设计
     */
    private boolean isPlaceholder = true;

    public void setPlaceholder(boolean placeholder) {
        isPlaceholder = placeholder;
    }

    public boolean isPlaceholder() {
        return isPlaceholder;
    }

    /**
     * 根据type选择布局方式
     *
     * @param data
     * @param context
     */
    public BaseMultiItemAdapter(@Nullable List<T> data, Context context) {
        super(data);
        this.context = context;
        //根据类型加载不同布局
        //addItemType(type,layoutId);
    }
    @Override
    protected void convert(BaseViewHolder helper, T item) {
        //添加点击监听(kotlin才需要，java里不需要，原因未知)
        //helper.addOnClickListener(R.id.item);
    }
}
