package com.yong.common.adapter;


import android.content.Context;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yong.common.R;

import java.util.List;

/**
 * @version 1.0
 * @Description: 默认单布局
 * @Author: yong
 * @time 2021/1/19 0019 11:01
 */
public class BaseAdapter<T> extends BaseQuickAdapter<T, BaseViewHolder> {
    private final static String TAG = "BaseAdapter";
    /**
     * 上下文
     */
    protected Context context;

    /**
     * 用于判断数据类型
     */
    protected int type;

    /**
     * 默认启用占位设计
     */
    protected boolean isPlaceholder = true;

    public void setPlaceholder(boolean placeholder) {
        isPlaceholder = placeholder;
    }

    public boolean isPlaceholder() {
        return isPlaceholder;
    }

    /**
     * 根据type选择布局方式
     *
     * @param layoutResId
     * @param data
     * @param context
     */
    public BaseAdapter(int layoutResId, @Nullable List<T> data, Context context,int type) {
        super(layoutResId, data);
        this.context = context;
        this.type = type;
    }

    @Override
    protected void convert(BaseViewHolder helper, T item) {
        //添加点击监听(kotlin才需要，java里不需要，原因未知)
        //helper.addOnClickListener(R.id.item);
    }
}
