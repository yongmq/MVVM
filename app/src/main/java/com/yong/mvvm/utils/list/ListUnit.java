package com.yong.mvvm.utils.list;

import android.app.Activity;
import android.view.View;

import androidx.fragment.app.Fragment;


import com.yong.mvvm.R;

import java.util.List;

import me.windleafy.kity.android.utils.view.list.ListTargetView;


public class ListUnit extends ListTargetView {

    private int LOADING_RES_ID = R.layout.view_loading;
    private int REFRESH_RES_ID = R.layout.view_refresh;
    private int NOTICE_RES_ID = R.layout.view_notice;

    private NoticeUnit mNoticeUnit;

    public ListUnit(Activity activity, int containerLayoutId) {
        super(activity, containerLayoutId);
        mNoticeUnit = new NoticeUnit(activity, NOTICE_RES_ID);
        initView();
    }

    public ListUnit(Fragment fragment, int containerLayoutId) {
        super(fragment, containerLayoutId);
        mNoticeUnit = new NoticeUnit(fragment, NOTICE_RES_ID);
        initView();
    }

    private void initView() {
        setNoticeView(mNoticeUnit.getNoticeView());
        setLoadingView(LOADING_RES_ID);
        setRefreshView(REFRESH_RES_ID);
    }


    public void notice(List list, CharSequence text) {
        mNoticeUnit.setNoticeText(text);
        notice(list);
    }

    public void notice(boolean show, CharSequence text) {
        mNoticeUnit.setNoticeText(text);
        notice(show);
    }

    public void notice(List list, int imageResid, CharSequence text) {
        mNoticeUnit.setNotice(imageResid, text);
        notice(list);
    }

    public void notice(boolean show, int imageResid, CharSequence text) {
        mNoticeUnit.setNotice(imageResid, text);
        notice(show);
    }

    public void notice(List list, CharSequence text, View.OnClickListener onClickListener) {
        mNoticeUnit.setNoticeText(text);
        notice(list, onClickListener);
    }

    public void notice(boolean show, CharSequence text, View.OnClickListener onClickListener) {
        mNoticeUnit.setNoticeText(text);
        notice(show, onClickListener);
    }

    public void notice(List list, int imageResid, CharSequence text, View.OnClickListener onClickListener) {
        mNoticeUnit.setNotice(imageResid, text);
        notice(list, onClickListener);
    }

    public void notice(boolean show, int imageResid, CharSequence text, View.OnClickListener onClickListener) {
        mNoticeUnit.setNotice(imageResid, text);
        notice(show, onClickListener);
    }

    public void noticeText(List list, CharSequence text) {
        mNoticeUnit.setNoticeTextOnly(text);
        notice(list);
    }

    public void noticeText(boolean show, CharSequence text) {
        mNoticeUnit.setNoticeTextOnly(text);
        notice(show);
    }

    public void noticeText(boolean show) {
        notice(show);
    }


}

