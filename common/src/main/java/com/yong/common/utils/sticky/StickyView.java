package com.yong.common.utils.sticky;

import android.view.View;

/**
 * @PackAge：com.gaokao.jhapp.yong.sticky
 * @author： yong
 * @Desc： 获取吸附View相关的信息
 * @Time: 2022/02/11 10:19:41
 **/
public interface StickyView {
    /**
     * 悬浮布局类型
     */
    public final static int STICKY_VIEW_TYPE = 0x000000111111;

    /**
     * 是否是吸附view
     * @param view
     * @return
     */
    boolean isStickyView(View view);

    /**
     * 得到吸附view的itemType
     * @return
     */
    int getStickViewType();
}
