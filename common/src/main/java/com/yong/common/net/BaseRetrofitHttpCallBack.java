package com.yong.common.net;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;
import com.yong.common.dialog.LoadingDialogUtil;
import com.yong.common.net.pojo.ApiResultNoData;
import com.yong.common.utils.ToastUtils;


/**
 * @Description: 自定义返回参数
 * @Author: yong
 * @time 2020/7/6 10:08
 * @Version: 1.0
 */
public abstract class BaseRetrofitHttpCallBack<T extends ApiResultNoData> implements ResultCallBack<T> {

    private static final String TAG = "RHttpCallBack";
    private static final int SUCCESS = 1001;

    /**
     * 权限不足(在其他地方登录)
     */
    private static final int JURISDICTION = 401;

    private Activity mActivity;

    private boolean isShowLoad = false;

    protected BaseRetrofitHttpCallBack(Activity activity) {
        mActivity = activity;
        showLoadingDialog();
    }

    protected BaseRetrofitHttpCallBack(Activity activity, boolean isShowLoad) {
        mActivity = activity;
        this.isShowLoad = isShowLoad;
        if (isShowLoad) {
            showLoadingDialog();
        }
    }

    @Override
    public void resultCallBack(T result) {
        Log.e(TAG, "resultCallBack: " + result.getCode());
        if (SUCCESS == result.getCode() || result.getCode() == 200) {
            onCodeSuccess(result);
        } else {
            onCodeFailure(result.getCode(), result, result.getMsg());
        }

        onFinish();

        if (isShowLoad) {
            dismissProgressDialog();
        }
    }

    @Override
    public void resultThrowableCallBack(Throwable e) {
        String msg = new Gson().toJson(e);
        if (msg.contains("failed to connect") || msg.contains("HTTP 500")) {
            ToastUtils.showLongToast(mActivity, "服务器内部错误");
            onError();
        } else if (msg.contains("HTTP 404")) {
            ToastUtils.showLongToast(mActivity, "资源不存在");
            onError();
        } else if (msg.contains("Network is unreachable")) {
            ToastUtils.showLongToast(mActivity, "网络无法连接");
            onError();
        } else if (msg.contains("Connection refused") || msg.contains("No route to host")) {
            ToastUtils.showLongToast(mActivity, "服务器连接失败");
            onError();
        } else if (msg.contains("403")) {
            ToastUtils.showLongToast(mActivity, "服务器拒绝请求(权限不足，请联系管理人员)");
            onError();
        } else if (msg.contains("connect timed out")) {
            ToastUtils.showLongToast(mActivity, "连接超时");
            onError();
        } else if (msg.contains(JURISDICTION + "")) {
            //token 失效
            //SPUtil.save(Constant.AUTO_LOGIN, false);
            //BaseApplication.i().getActivityManage().finishAll();
            //ARouter.getInstance().build(ConstantRouterUrl.ACTIVITY_LOGIN).navigation();
        } else {
            ToastUtils.showLongToast(mActivity, msg);
            onError();
        }

        onFinish();

        if (isShowLoad) {
            dismissProgressDialog();
        }
    }

    /**
     * 请求失败返回
     */
    protected void onError() {

    }

    /**
     * 接口请求完毕
     */
    protected void onFinish(){

    }

    /**
     * 当请求成功调用此方法
     *
     * @param data
     */
    public abstract void onCodeSuccess(T data);

    public void onCodeFailure(int code, T data, String message) {
        ToastUtils.showLongToast(mActivity, message);
        onError();
    }

    private void showLoadingDialog() {
        //启动加载动画
        LoadingDialogUtil.getInstance().showLoadingDialog(mActivity);
    }

    private void dismissProgressDialog() {
        //关闭动画
        LoadingDialogUtil.getInstance().closeLoadingDialog();
    }

}
