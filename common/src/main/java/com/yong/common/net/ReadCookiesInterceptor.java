package com.yong.common.net;

import android.util.Log;
import com.google.gson.Gson;
import com.yong.common.utils.SPUtils;

import java.io.IOException;
import java.util.HashSet;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * @Description: 查看cookie
 * @Author: yong
 * @time 2020/7/27 17:10
 * @Version: 1.0
 */
public class ReadCookiesInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request.Builder builder = chain.request().newBuilder();
        HashSet preferences = new Gson().fromJson(SPUtils.getInstance().getString("cookie"), HashSet.class);
        if (preferences != null) {
            for (Object cookie : preferences) {
                builder.addHeader("Cookie", (String) cookie);
                // This is done so I know which headers are being added; this interceptor is used after the normal logging of OkHttp
                Log.v("OkHttp", "Adding Header: " + cookie);
            }
        }
        return chain.proceed(builder.build());
    }

}