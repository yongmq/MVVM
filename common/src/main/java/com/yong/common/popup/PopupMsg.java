package com.yong.common.popup;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import razerdp.basepopup.BasePopupWindow;
import razerdp.util.animation.AnimationHelper;
import razerdp.util.animation.ScaleConfig;

/**
 * @version 1.0
 * @Description: 消息框
 * @Author: yong
 * @time 2021/6/22 0022 14:37
 */
public class PopupMsg extends BasePopupWindow {
    public static int id;

    public PopupMsg(Context context) {
        super(context);
    }

    @NotNull
    @Contract("_, _ -> new")
    public synchronized static PopupMsg getInstance(Context context, int layoutId) {
        id = layoutId;
        return new PopupMsg(context);
    }

    @Override
    public View onCreateContentView() {
        return createPopupById(id);
    }

    @Override
    protected Animation onCreateShowAnimation() {
        return AnimationHelper.asAnimation()
                .withScale(ScaleConfig.CENTER)
                .toShow();
    }


    @Override
    protected Animation onCreateDismissAnimation() {
        return AnimationHelper.asAnimation()
                .withScale(ScaleConfig.CENTER)
                .toDismiss();
    }
}
