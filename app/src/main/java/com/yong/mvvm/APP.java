package com.yong.mvvm;

import com.yong.buglyutil.BuglyUtil;
import com.yong.common.BaseApplication;

/**
 * @PackAge：com.yong.mvvm
 * @Author： yong
 * @Desc：APP
 * @Time: 2022/02/17 10:38:30
 **/
public class APP extends BaseApplication {
    @Override
    public void onCreate() {
        super.onCreate();
        //不开启自动检查
        BuglyUtil.autoCheckUpgrade(false);
        BuglyUtil.enableCusUpgradeDialog();
        //bugly初始化
        BuglyUtil.init(getApplicationContext(),"6b3402d9b7",false);

    }
}
