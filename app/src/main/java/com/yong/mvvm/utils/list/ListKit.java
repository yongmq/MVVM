package com.yong.mvvm.utils.list;

import android.app.Activity;
import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;


import com.yong.mvvm.R;

import java.util.List;

import me.windleafy.kity.android.utils.view.TargetView;
import me.windleafy.kity.android.utils.view.TargetViewKit;
import me.windleafy.kity.android.utils.view.list.ListTargetViewKit;

public class ListKit extends ListTargetViewKit {

    private static final int loadingLayout = R.layout.view_loading;
    private static final int refreshLayout = R.layout.view_refresh;
    private static final int emptyLayout = R.layout.view_notice;

    private ListKit() {
        throw new UnsupportedOperationException("cannot be instantiated");
    }

    /**
     * showLoading
     */
    public static void showLoading(Activity activity, int containerLayoutId) {
        showLoading(activity, containerLayoutId, loadingLayout);
    }

    public static void showLoading(Activity activity, View containerLayoutView) {
        showLoading(activity, containerLayoutView, loadingLayout);
    }

    public static void showLoading(Fragment fragment, int containerLayoutId) {
        showLoading(fragment, containerLayoutId, loadingLayout);
    }

    public static void showLoading(Fragment fragment, View containerLayoutView) {
        showLoading(fragment, containerLayoutView, loadingLayout);
    }

    /**
     * hideLoading
     */
    public static void hideLoading(Activity activity, int containerLayoutId) {
        hideLoading(activity, containerLayoutId, loadingLayout);
    }

    public static void hideLoading(Activity activity, View containerLayoutView) {
        hideLoading(activity, containerLayoutView, loadingLayout);
    }

    public static void hideLoading(Fragment fragment, int containerLayoutId) {
        hideLoading(fragment, containerLayoutId, loadingLayout);
    }

    public static void hideLoading(Fragment fragment, View containerLayoutView) {
        hideLoading(fragment, containerLayoutView, loadingLayout);
    }


    /**
     * showRefresh
     */
    public static void showRefresh(Activity activity, int containerLayoutId, boolean cancelable) {
        showRefresh(activity, containerLayoutId, cancelable, false);
    }

    public static void showRefresh(Activity activity, View containerLayoutView, boolean cancelable) {
        showRefresh(activity, containerLayoutView, cancelable, false);
    }

    public static void showRefresh(Fragment fragment, int containerLayoutId, boolean cancelable) {
        showRefresh(fragment, containerLayoutId, cancelable, false);
    }

    public static void showRefresh(Fragment fragment, View containerLayoutView, boolean cancelable) {
        showRefresh(fragment, containerLayoutView, cancelable, false);
    }

    public static void showRefresh(Activity activity, int containerLayoutId) {
        showRefresh(activity, containerLayoutId, null, false);
    }

    public static void showRefresh(Activity activity, View containerLayoutView) {
        showRefresh(activity, containerLayoutView, null, false);
    }

    public static void showRefresh(Fragment fragment, int containerLayoutId) {
        showRefresh(fragment, containerLayoutId, null, false);
    }

    public static void showRefresh(Fragment fragment, View containerLayoutView) {
        showRefresh(fragment, containerLayoutView, null, false);
    }

    public static void showRefreshParentFrame(Activity activity, int containerLayoutId, boolean cancelable) {
        showRefresh(activity, containerLayoutId, cancelable, true);
    }

    public static void showRefreshParentFrame(Activity activity, View containerLayoutView, boolean cancelable) {
        showRefresh(activity, containerLayoutView, cancelable, true);
    }

    public static void showRefreshParentFrame(Fragment fragment, int containerLayoutId, boolean cancelable) {
        showRefresh(fragment, containerLayoutId, cancelable, true);
    }

    public static void showRefreshParentFrame(Fragment fragment, View containerLayoutView, boolean cancelable) {
        showRefresh(fragment, containerLayoutView, cancelable, true);
    }

    public static void showRefreshParentFrame(Activity activity, int containerLayoutId) {
        showRefresh(activity, containerLayoutId, null, true);
    }

    public static void showRefreshParentFrame(Activity activity, View containerLayoutView) {
        showRefresh(activity, containerLayoutView, null, true);
    }

    public static void showRefreshParentFrame(Fragment fragment, int containerLayoutId) {
        showRefresh(fragment, containerLayoutId, null, true);
    }

    public static void showRefreshParentFrame(Fragment fragment, View containerLayoutView) {
        showRefresh(fragment, containerLayoutView, null, true);
    }

    public static void showRefresh(Activity activity, int containerLayoutId, Boolean cancelable, boolean isParentFrame) {
        showRefresh(activity, containerLayoutId, refreshLayout, cancelable, isParentFrame);
    }

    public static void showRefresh(Activity activity, View containerLayoutView, Boolean cancelable, boolean isParentFrame) {
        showRefresh(activity, containerLayoutView, refreshLayout, cancelable, isParentFrame);
    }

    public static void showRefresh(Fragment fragment, int containerLayoutId, Boolean cancelable, boolean isParentFrame) {
        showRefresh(fragment, containerLayoutId, refreshLayout, cancelable, isParentFrame);
    }

    public static void showRefresh(Fragment fragment, View containerLayoutView, Boolean cancelable, boolean isParentFrame) {
        showRefresh(fragment, containerLayoutView, refreshLayout, cancelable, isParentFrame);
    }


    /**
     * hideRefresh
     */
    public static void hideRefresh(Activity activity, int containerLayoutId) {
        hideRefresh(activity, containerLayoutId, false);
    }

    public static void hideRefresh(Activity activity, View containerLayoutView) {
        hideRefresh(activity, containerLayoutView, false);
    }

    public static void hideRefresh(Fragment fragment, int containerLayoutId) {
        hideRefresh(fragment, containerLayoutId, false);
    }

    public static void hideRefresh(Fragment fragment, View containerLayoutView) {
        hideRefresh(fragment, containerLayoutView, false);
    }

    public static void hideRefreshParentFrame(Activity activity, int containerLayoutId) {
        hideRefresh(activity, containerLayoutId);
    }

    public static void hideRefreshParentFrame(Activity activity, View containerLayoutView) {
        hideRefresh(activity, containerLayoutView);
    }

    public static void hideRefreshParentFrame(Fragment fragment, int containerLayoutId) {
        hideRefresh(fragment, containerLayoutId);
    }

    public static void hideRefreshParentFrame(Fragment fragment, View containerLayoutView) {
        hideRefresh(fragment, containerLayoutView);
    }

    public static void hideRefresh(Activity activity, int containerLayoutId, boolean isParentFrame) {
        hideRefresh(activity, containerLayoutId, refreshLayout, isParentFrame);
    }

    public static void hideRefresh(Activity activity, View containerLayoutView, boolean isParentFrame) {
        hideRefresh(activity, containerLayoutView, refreshLayout, isParentFrame);
    }

    public static void hideRefresh(Fragment fragment, int containerLayoutId, boolean isParentFrame) {
        hideRefresh(fragment, containerLayoutId, refreshLayout, isParentFrame);
    }

    public static void hideRefresh(Fragment fragment, View containerLayoutView, boolean isParentFrame) {
        hideRefresh(fragment, containerLayoutView, refreshLayout, isParentFrame);
    }


    /**
     * setEmpty
     */
    public static void setEmpty(Fragment fragment, List list, int containerLayoutId, View.OnClickListener listener) {
        if (list.size() == 0) {
            TargetViewKit.addView(fragment, containerLayoutId, emptyLayout, TargetView.AddType.REPLACE, null, listener);
        } else {
            TargetViewKit.removeView(fragment, containerLayoutId, emptyLayout, TargetView.AddType.REPLACE);
        }
    }

    public static void setEmpty(Activity activity, List list, int containerLayoutId, View.OnClickListener listener) {
        if (list.size() == 0) {
            TargetViewKit.addView(activity, containerLayoutId, emptyLayout, TargetView.AddType.REPLACE, null, listener);
        } else {
            TargetViewKit.removeView(activity, containerLayoutId, emptyLayout, TargetView.AddType.REPLACE);
        }
    }

    /**
     * 向上滑动一条Item高度
     *
     * @param recyclerView
     * @param position     依据滑动的Item
     */
    public static void smoothScrollByItem(RecyclerView recyclerView, int position) {
        int height = recyclerView.getLayoutManager().findViewByPosition(position).getMeasuredHeight();
        recyclerView.smoothScrollBy(0, height);
    }

    /**
     * 向上滑动半条Item高度
     *
     * @param recyclerView
     * @param position     依据滑动的Item
     */
    public static void smoothScrollByItemHalf(RecyclerView recyclerView, int position) {
        int height = recyclerView.getLayoutManager().findViewByPosition(position).getMeasuredHeight();
        recyclerView.smoothScrollBy(0, height / 2);
    }

    /**
     * 向上滑动半条Item高度
     *
     * @param recyclerView
     * @param allList      数据
     * @param newList      新增数据
     */
    public static void smoothScrollByItemHalf(RecyclerView recyclerView, List allList, List newList) {
        if (allList != null && newList != null && allList.size() > newList.size()) {
            ListKit.smoothScrollByItem(recyclerView, allList.size() - newList.size());
        }
    }

}
