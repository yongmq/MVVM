package com.yong.common.net.pojo

/**
 * @Description: 带data数据的返回值
 * @Author: yong
 * @time 2020/6/3 16:05
 * @Version: 1.0
 */
class ApiResult<T> : ApiResultNoData() {
     val data: T? = null
}